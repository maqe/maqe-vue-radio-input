# @maqe-vue/radio-input
The Vue2 component for radio-input

![label-insde](https://i.imgur.com/RcrX71S.png)

See demo on: [Storybook](https://maqe-vue.qa.maqe.com/?path=/story/radio--all)

------

## Installation

#### NPM
Install the npm package.

```
npm install @maqe-vue/radio-input --save
```

#### Register the component

```
import Input from '@maqe-vue/radio-input'
import '@maqe-vue/radio-input/dist/style.css'

Vue.component('vmq-radio', Input)
```

------

## Usage

#### Basic
<img src="https://i.imgur.com/Lt6jrQv.png" height="28">

```
<vmq-radio
    v-model="radio"
    :list="[
        { value: 'yes', title: 'Yes' },
        { value: 'no', title: 'No' }
    ]"
/>
```

------

## API
#### Props

| Name                 | Type                | Description    | default    |
| :--------------------|:-------------------:|----------------|:-----------|
| `v-model`            | `bind`              |                |            |
| `list`               | `array`             | Radio group including list | `[]`
| `list > value`       | `String | Integer`  |                | `null`
| `list > title`       | `String`            | Label name     | `""`
| `list > disabled`    | `Boolean`           |                | `false`    |
| `containerClass`     | `string`

## Style
#### Custom Style
Custom style with css variable

<img src="https://i.imgur.com/3phOMSM.png" height="100">

```
<vmq-radio
    v-model="radio"
    :list="[
        { value: 'yes', title: 'Yes' },
        { value: 'no', title: 'No' }
    ]"
/>

// for example to set as a global
<style>
    :root {
        --vmq-radio-color: tan;
    }
</style>
```